// SIMPLE WEB CRAWLER NODEJS
// GETTING TOP 100 MOVIES AND THEIR RATINGS FROM IMDB
// BY: NATAN DE OLIVEIRA PEREIRA DA SILVA - NATAN673@GMAIL.COM


var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var express = require('express'),
    app = express();

    app.get('/', function(req, res) {
        fs.readFile("web/index.html", function (error, pgResp) {
            if (error) {
                res.writeHead(404);
                res.write('Contents you are looking are Not Found');
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pgResp);
            }

            res.end();
    })})

    app.get('/movies', function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        request('http://www.imdb.com/chart/moviemeter', function (err, res, body) {
            if (err){
                console.log('Erro: ' + err)
            }else{
                var arr = [];
                var $ = cheerio.load(body);

                $('.lister-list tr').each(function () {
                    var title = $(this).find('.titleColumn a').text().trim();
                    var rating = $(this).find('.imdbRating strong').text().trim();
                    if (rating == "") rating = "N/A";

                    var objJSON = {
                        "Title": title,
                        "Rating": rating
                    }
                    arr.push(objJSON);
                });
                var topMovies = { arr };
                print(topMovies);
            }
        });
        function print(retorno) {
            res.json(retorno);
        }
    })

    var server = app.listen(3000);
    console.log('Servidor Express iniciado na porta %s', server.address().port);